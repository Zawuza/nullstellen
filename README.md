# Nullstellen #

### Was ist das ###

* Das ist mein kleines Programm, das die Nullstellen des Polynoms des zweiten Grades mithilfe von Newton-Verfahren sucht.

### What is it ###

* It's a small program that searches for zeros of a quadratic function (Newtons method)