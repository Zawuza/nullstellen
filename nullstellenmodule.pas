unit nullstellenmodule;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type TWert = record
      x1:integer;
      x2:integer;
      Is1nullstelle:boolean;
      Is2nullstelle:boolean;
      end;

function FindNulstellen(a,b,c:real): string;
function Funktionswert(a,b,c,wert:real): real;
procedure Ableitung(a,b: real;var aba,abb: real);
function Werte(a,b,c:real; zweinullstellen:boolean): TWert;
function Diskriminant(a,b,c:real): real;
function Nullstelle(a,b,c,aba,abb,wert:real):double;

implementation

function Funktionswert(a,b,c,wert:real): real;
begin
   Result:=(a*sqr(wert))+(b*wert)+c;
end;

procedure Ableitung(a,b: real;var aba,abb: real); {Ableitung ziehen}
begin
   aba:=a*2;
   abb:=b;
end;

function Werte(a,b,c:real; zweinullstellen:boolean): TWert;
var links, rechts: longint;
    x1,x:real;
    {x1 - Speicherung von x von f(links) und f(rechts)}
    zeiger: integer;
    ampel:boolean;
{Zwei Zeiger: links und rechts bewegen in verschiedenen Richtungen der x-Achse (Schritt für schritt
nacheinander (ampel reguliert das)) und prüfen, ob zwei Werte, die daneben stehen, sich von verschiedenen Seiten
befinden. Wenn ja, dann speichern wir dieses Wert}
begin
   zeiger:=1;
   links:=0;
   rechts:=0;
   ampel:=true;
   if Funktionswert(a,b,c,0)=0 then
      begin
           Result.x1:=0;
           Result.Is1nullstelle:=true;
      end;
   {Schleife, in dem man Werte sucht}
   while (links<>low(longint)) or (rechts<>high(longint)) do
         begin
          if ampel=true then
             begin
             x:=Funktionswert(a,b,c,links);
             Dec(links);
             x1:=Funktionswert(a,b,c,links);
             if ((x<0) and (x1>=0)) or ((x>0) and (x1<=0)) then
                begin
                if zeiger=1 then
                   begin
                   Result.x1:=links;
                   Inc(zeiger);
                   if Funktionswert(a,b,c,links)=0 then Result.Is1nullstelle:=true;
                   if zweinullstellen=false then exit
                   end
                else
                   begin
                        Result.x2:=links;
                        if Funktionswert(a,b,c,links)=0 then Result.Is2nullstelle:=true;
                        exit;
                   end;
                end;
             end
          else
             begin
             x:=Funktionswert(a,b,c,rechts);
             Inc(rechts);
             x1:=Funktionswert(a,b,c,rechts);
             if ((x<0) and (x1>=0)) or ((x>0) and (x1<=0)) then
                begin
                if zeiger=1 then
                   begin
                   Result.x1:=rechts;
                   Inc(zeiger);
                   if Funktionswert(a,b,c,rechts)=0 then Result.Is1nullstelle:=true;
                   if zweinullstellen=false then exit
                   end
                else
                   begin
                        Result.x2:=rechts;
                        if Funktionswert(a,b,c,rechts)=0 then Result.Is2nullstelle:=true;
                        exit;
                   end;
                end;
             end;
             ampel:=not ampel;
         end;
end;

function Diskriminant(a,b,c:real): real;
begin
   Result:=sqr(b)-(4*a*c);
end;

function Nullstelle(a,b,c,aba,abb,wert:real):double;
var xn,x:double;
begin
   x:=wert;
   if (aba*x+abb)=0 then
      begin
      Result:=x;
      exit;
      end;
   xn:=x-(Funktionswert(a,b,c,x)/(aba*x+abb));
   while xn<>x do
         begin
            x:=xn;
            if aba*x+abb=0 then break;
            xn:=x-(Funktionswert(a,b,c,x)/(aba*x+abb));
         end;
   Result:=x;
   end;

function FindNulstellen(a,b,c:real): string;
var aba,abb:real;       {Ableitung}
    Wert:TWert;         {Wertenblock}
    nullstell:boolean;
begin
  aba:=0;
  abb:=0;
  if Diskriminant(a,b,c)<0 then
     begin
          Result:='Keine Nullstelle';
          exit;
     end;
  if Diskriminant(a,b,c)=0 then
       begin
       nullstell:=false;
       Wert.x1:=0;
       end
  else begin
       nullstell:=true;
       Wert:=Werte(a,b,c,nullstell);
       end;
  Ableitung(a,b,aba,abb);
  Result:='Nullstellen: ' + #13;
  Result:=Result + FloatToStr(Nullstelle(a,b,c,aba,abb,Wert.x1)) + #13;
  if nullstell then Result:= Result + FloatToStr(Nullstelle(a,b,c,aba,abb,Wert.x2));
end;

end.

